using Microsoft.AspNetCore.Mvc;

namespace hub_timer.Controllers {

    [Route("api/message")]
    public class MessageController : Controller {

        [HttpPost]
        public IActionResult Post() {
            //broadcast message to the clients
            return Ok();
        }
    }
}