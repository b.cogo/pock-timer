using System;

public class TxtService {

    private string[] valor;

    public void GravarTexto(string texto, Boolean inicio) {
        this.valor = texto.Split('.');
        System.IO.File.WriteAllLines((inicio ? @"C:\DEV\pock-timer\documentotxt.txt" : @"C:\DEV\pock-timer\documentofinaltxt.txt"), valor);
    }

}