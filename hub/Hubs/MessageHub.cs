using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace hub_timer {
    public class MessageHub : Hub {

        private DateTime dataInicialConexao;
        private DateTime dataFinalConexao;
        private TxtService txtservice = new TxtService();
        public void SetRealTime() {  
            this.dataInicialConexao = DateTime.Now;
            txtservice.GravarTexto(Context.ConnectionId + "." +  this.dataInicialConexao.ToString(), true); 
        }

        public async Task desconectar() { 
            await this.OnDisconnectedAsync(new Exception());
        }

        public override async Task OnConnectedAsync() {  
            await Clients.Caller.SendAsync("connected"); 
        }
        public override async Task OnDisconnectedAsync(Exception ex) {
            this.dataFinalConexao = DateTime.Now;
            txtservice.GravarTexto(Context.ConnectionId + "." +  this.dataFinalConexao.ToString(), false);  
            await Clients.Caller.SendAsync("disconnected", this.dataFinalConexao);  
        }
    }
    
}