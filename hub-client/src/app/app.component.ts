import { Component, OnInit } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    private _hubConnection: HubConnection;
    private async: any;
    private data1: Date;
    private data2: Date;
    private myInterval: any;
    private timerCancelado: Boolean;
    private myTimeout: any;
    private elemento: HTMLElement;


    public comecarTimer(): void {
        this.data2 = new Date(); 
        this.elemento = document.getElementById('id');       
        this.elemento.innerText = this.data2.getHours() + ':' + this.data2.getMinutes() + ':' + this.data2.getSeconds();
    }        

    ngOnInit(): void {
        
        this._hubConnection = new HubConnectionBuilder()
                                    .withUrl('http://localhost:5000/message')
                                    .build();

        

        this._hubConnection.start()
            .then(() => {
                console.log('Hub connection started')
                this.data1 = new Date();
                this.myInterval = setInterval(this.comecarTimer, 1000);                
            }).catch((error) => { console.log(error); });
            
            this._hubConnection.on('connected', () => {
                this._hubConnection.invoke('SetRealTime');
                this.myTimeout = setTimeout(this.timerMouse, 5000, this._hubConnection);                    
        });

        this._hubConnection.on('disconnected', (data: any) => {
            clearInterval(this.myInterval);
            clearInterval(this.myTimeout);
            this.elemento = document.getElementById('id');
            this.elemento.innerText = 'data saída -> ' + this.formatarData(data.toString());
            this.timerCancelado = true;
        });

        document.addEventListener("mousemove", this.mouseMove, false);
        document.addEventListener("click", this.click, false);
    }

    public sendData() {
        if (!this.timerCancelado) {
            clearTimeout(this.myTimeout);
            this.myTimeout = setTimeout(this.timerMouse, 5000, this._hubConnection);
        }
    }

    public timerMouse(conn: HubConnection) :void {
        this.data2 = new Date();
        this.timerCancelado = true;
        conn.invoke('desconectar');        
    }

    public formatarData(data: string): string {
        return data.substr(11, 8);
    }

    public mouseMove = (event: MouseEvent): void => {
        this.sendData();
    }

    public click = (event: MouseEvent): void => {
        this.sendData();
    }

}
