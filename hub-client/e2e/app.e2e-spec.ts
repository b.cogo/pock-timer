import { HubTimerclientPage } from './app.po';

describe('hub-timerclient App', function() {
  let page: HubTimerclientPage;

  beforeEach(() => {
    page = new HubTimerclientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
